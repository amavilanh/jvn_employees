import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/Employee';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api/api.service';
import { AlertService } from '../../services/alert/alert.service';


@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {

  constructor(private router: Router, private api: ApiService, private alert: AlertService) { }

  employees: Employee;
  newuserForm = new FormGroup<any>({
    papellido: new FormControl('', Validators.required),
    pnombre: new FormControl('', Validators.required),
    onombres: new FormControl('', Validators.required),
    nempleo: new FormControl('', Validators.required),
  });

  ngOnInit(): void {
    // Agrega el suscriptor al evento de cambio de valor para convertir a mayúsculas
    this.newuserForm.get('papellido').valueChanges.subscribe(value => {
      this.newuserForm.get('papellido').setValue(value.toUpperCase(), { emitEvent: false });
    });
    this.newuserForm.get('pnombre').valueChanges.subscribe(value => {
      this.newuserForm.get('pnombre').setValue(value.toUpperCase(), { emitEvent: false });
    });
    this.newuserForm.get('onombres').valueChanges.subscribe(value => {
      this.newuserForm.get('onombres').setValue(value.toUpperCase(), { emitEvent: false });
    });
    this.newuserForm.get('nempleo').valueChanges.subscribe(value => {
      this.newuserForm.get('nempleo').setValue(value.toUpperCase(), { emitEvent: false });
    });
  }

  postForm(form: Employee) {
    //console.log(form);
    this.api.postEmployee(form).subscribe(data => {
      console.log(data);
      if (data = !null) {
        this.alert.showSuccess('Usuario Agregado', 'Hecho!')
        this.router.navigate(['dashboard'])
      } else {
        this.alert.showError('Error', 'Error!')
      }
    })
  }

  regresar() {
    this.router.navigate(['dashboard']);
  }

}