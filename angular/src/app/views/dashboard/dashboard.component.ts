import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import { Employee } from 'src/app/models/Employee';
import { Router } from '@angular/router';
import { NgLocalization } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  employees:Employee[];

  constructor(private api:ApiService, private router:Router){}

  ngOnInit(){
    this.api.getEmployees()
    .subscribe(data=>{
      this.employees=data;
    })
  }

  editar(id:any){
    this.router.navigate(['edit',id])
  }

  eliminar(id:any){
    if (confirm('¿Está seguro de que desea eliminar el empleado?')) {
      this.api.deleteEmployee(id).subscribe(data =>{
        location.reload();
      })
    }
    
  }

  agregar(){
    this.router.navigate(['new']);
  }

}
