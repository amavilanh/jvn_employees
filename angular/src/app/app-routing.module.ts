import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { NewComponent } from './views/new/new.component';
import { EditComponent } from './views/edit/edit.component';

const routes: Routes = [
  {path:'' , redirectTo:'dashboard' , pathMatch:'full'},
  {path:'dashboard', component:DashboardComponent},
  {path:'edit/:id', component:EditComponent},
  {path:'new', component:NewComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents= [NewComponent, DashboardComponent, EditComponent]
