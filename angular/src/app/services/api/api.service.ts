import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { Employee } from 'src/app/models/Employee';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url:String = "http://localhost:8080/employee/";

  constructor(private http:HttpClient) { }
  
   getEmployee(id:any){
    let link = this.url + "list/" + id;
    return this.http.get<Employee>(link);
  }

  getEmployees(){
    let link = this.url + "list/";
    return this.http.get<Employee[]>(link);
  }

  putEmployee(form:Employee){
    let link = this.url + '';
    return this.http.put(link, form);
  }

  deleteEmployee(ide:number){
    let link = this.url + "delete/" + ide;
    let Options = {
      headers: new HttpHeaders({
        'Conten-type': 'application/json'
      }),
      body:ide
    }
    return this.http.delete(link, Options);
  }

  postEmployee(form:Employee){
    let link = this.url + 'save/';
    return this.http.post(link,form);
  }
  //o podria haber llamado la url http://localhost:8080/employee/1?id=1 y ya....

  
}
