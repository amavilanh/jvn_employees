import { Injectable } from '@angular/core';
import {ToastrService} from 'ngx-toastr'


@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private toast:ToastrService) { }


  showSuccess(titulo: string | undefined, texto:string | undefined){
    this.toast.success(texto,titulo);
  }

  showError(titulo: string | undefined, texto:string | undefined){
    this.toast.error(texto,titulo);
  }

}
