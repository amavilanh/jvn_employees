export interface Employee{
    id:number;
    papellido:string;
    onombres:string;
    nempleo:string;
    pnombre:string;
    correo:string;
}