/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.jvn.employee.Commons;
import java.io.Serializable;
import java.util.List;
/**
 *
 * @author Andres
 */
public interface GenericService<T, ID extends Serializable> {
    T save(T entity);
    void delete(ID id);
    T get(ID id);
    List<T> getAll();
}

