/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.jvn.employee.Service.Implement;
import com.jvn.employee.Models.Employee;
import com.jvn.employee.Dao.EmployeeDao;
import com.jvn.employee.Service.EmployeeService;
import com.jvn.employee.Commons.GenericServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author Andres
 */
@Service
public class EmployeeServiceImpl extends GenericServiceImpl<Employee, Integer> implements EmployeeService{
    @Autowired
    private EmployeeDao EmployeeDao;

    @Override
    public CrudRepository<Employee, Integer> getDao() {
        return EmployeeDao;
    }
    
}
