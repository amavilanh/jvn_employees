/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.jvn.employee.Service;
import com.jvn.employee.Commons.GenericService;
import com.jvn.employee.Models.Employee;
import org.springframework.stereotype.Service;

/**
 *
 * @author Andres
 */
@Service
public interface EmployeeService extends GenericService<Employee, Integer>{
    
}
