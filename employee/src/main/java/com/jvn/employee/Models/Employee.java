/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.jvn.employee.Models;
import java.io.Serializable; //Convierte un objeto a bytes para poder enviarlo por la web
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Andres
 */
@Entity
@Table(name = "employee")   
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int Id; 
    @Column(name="apellido")
    private String PApellido;
    @Column(name="nombre")
    private String PNombre;
    @Column(name="onombres")
    private String ONombres;
    @Column(name="nempleo")
    private String NEmpleo;
    @Column(name="correo")
    private String correo;

    public Employee() {
    }

    public Employee(int Id, String PApellido, String PNombre, String ONombres, String NEmpleo, String Correo) {
        this.Id = Id;
        this.PApellido = PApellido;
        this.PNombre = PNombre;
        this.ONombres = ONombres;
        this.NEmpleo = NEmpleo;
        this.correo = Correo;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getPApellido() {
        return PApellido;
    }

    public void setPApellido(String PApellido) {
        this.PApellido = PApellido;
    }

    public String getPNombre() {
        return PNombre;
    }

    public void setPNombre(String PNombre) {
        this.PNombre = PNombre;
    }

    public String getONombres() {
        return ONombres;
    }

    public void setONombres(String ONombres) {
        this.ONombres = ONombres;
    }

    public String getNEmpleo() {
        return NEmpleo;
    }

    public void setNEmpleo(String NEmpleo) {
        this.NEmpleo = NEmpleo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String Correo) {
        this.correo = Correo;
    }
    
}
