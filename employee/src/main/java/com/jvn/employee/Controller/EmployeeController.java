/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.jvn.employee.Controller;
import com.jvn.employee.Models.Employee;
import com.jvn.employee.Service.EmployeeService;
import com.jvn.employee.Dao.EmployeeDao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andres
 */
@RestController
@RequestMapping("/employee")
@CrossOrigin("*")
public class EmployeeController {
    
    private final EmployeeDao employeeDao;
    public EmployeeController(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }
    
    @Autowired
    private EmployeeService EmployeeService;
    
    @PostMapping(value="/save")
    public ResponseEntity<Employee> agregar(@RequestBody Employee employee){
        String correo = "";     
        Employee obj = EmployeeService.save(employee);
        if (obj != null) {
            // La operación de guardado fue exitosa
            if(obj.getNEmpleo().equals("COLOMBIA")){
                correo=obj.getPNombre()+"."+obj.getPApellido()+"@jvntecnologias.com";
            }else{
                correo=obj.getPNombre()+"."+obj.getPApellido()+"@jvntecnologias.com.us";
            }
            if(employeeDao.findByCorreo(correo)!= null){
                correo=obj.getPNombre()+"."+obj.getPApellido()+"."+obj.getId()+"@jvntecnologias.com.us";
            }
            obj.setCorreo(correo);
            EmployeeService.save(obj);
            return new ResponseEntity<>(obj, HttpStatus.OK);
        } else {
            // La operación de guardado falló
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @DeleteMapping(value="/delete/{id}")
    public ResponseEntity<Employee> eliminar(@PathVariable int id){
        Employee obj = EmployeeService.get(id);
        if(obj!=null)
            EmployeeService.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Employee> editar(@RequestBody Employee employee){
        Employee obj= EmployeeService.get(employee.getId());
        if(obj!=null){
            obj.setPNombre(employee.getPNombre());
            obj.setPApellido(employee.getPApellido());
            obj.setONombres(employee.getONombres());
            obj.setNEmpleo(employee.getNEmpleo());
            EmployeeService.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Employee> VerTodo(){
        return EmployeeService.getAll();
    }
    
    @GetMapping("/list/{id}")
    public Employee VerporId(@PathVariable int id){
        return EmployeeService.get(id);
    }
    
}
